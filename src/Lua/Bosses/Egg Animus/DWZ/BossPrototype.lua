/* Main DWZ Animus code */

-- First of all we add this into the model_mobj list for the model viewer
model_mobj[255] = {name = "Egg Animus (DWZ)"}

-- Setup for all the boss components
addHook('MobjSpawn', function(mobj)
	if mobj.obj_table == nil then
		mobj.obj_table = {
			body_main = {},
			limb_tail = {},
			group_rarm = {
				glimb_bcryst = {},
				limb_joint = {},
				glimb_gcryst = {},
				limb_hand = {}
			},
			group_larm = {
				glimb_ycryst = {},
				limb_joint = {},
				glimb_rcryst = {},
				limb_hand = {}
			},
		}
	end

	local obj = mobj.obj_table

	-- Creating the 3 tail objects
	MDL_spawnObject(3, limb_tail, mobj, MT_DWZANI_TALSEG)

	--  catch these hands
	MDL_spawnObject(1, group_rarm.limb_hand, mobj, MT_DWZANI_HANSEG)
	MDL_spawnObject(1, group_larm.limb_hand, mobj, MT_DWZANI_HANSEG)

	-- Create 2 arm joints

	-- Create 4 crystals out of splats and papersprites (oh fuck me)

	-- Position literally everything for our initial start


	-- Set our Zlimit here
	if mobj.zlimit == nil then
		mobj.zlimit = mobj.z
	end
end, MT_DWZ_EGGANIMUS)

-- Function code for keeping our 3d crystals positioned properly

-- Main boss code
addHook('MobjThinker', function(mo)
	-- Always point at the player
	mo.angle = R_PointToAngle2(mo.x, mo.y, mo.target.x, mo.target.y)

	-- We can raise higher but should never go lower than our limit
	if mo.z < mo.zlimit then
		mo.z = mo.zlimit
	end
end, MT_DWZ_EGGANIMUS)