-- Mobj
freeslot(
    'MT_DWZ_EGGANIMUS',
    'MT_DWZANI_APRISM',
    'MT_DWZANI_TALSEG',
    'MT_DWZANI_AJOINT',
    'MT_DWZANI_AHAND',
    'MT_SEGMENT',

    'S_ANIBODY_IDLE',
    'S_ANITAIL',
    'S_ANIHAND',
    'S_ANIPRISM',
    'S_SEGMENT',

    'spr_eclw',
    'spr_elim',
    'spr_eprs',
    'spr_eps1',
    'spr_eps2',
    'spr_eps3',
    'spr_eps4',
    'spr_etal',
    'spr_warn'
)

-- Define Mobjs
mobjinfo[MT_DWZ_EGGANIMUS] = {
    --$Name Egg-Animus (Dimension Warp)
    --$Sprite EPRSA0
    --$Category Mystic Realm Bosses
    doomednum = 255,
    spawnstate = S_ANIBODY_IDLE,
    seestate = S_ANIBODY_IDLE,
    painstate = S_ANIBODY_IDLE,
    spawnhealth = 20,
    reactiontime = 2,
    painchance = 0,
    speed = 30*FRACUNIT,
    radius = 24*FRACUNIT,
    height = 46*FRACUNIT,
    damage = 20*FRACUNIT,
    flags = MF_SPECIAL|MF_SHOOTABLE|MF_NOGRAVITY|MF_FLOAT|MF_BOSS
}

mobjinfo[MT_DWZANI_TALSEG] = {
    doomednum = -1,
    spawnstate = S_ANITAIL,
    radius = 24*FRACUNIT,
    mass = 8*FRACUNIT,
    flags = MF_NOCLIP|MF_NOBLOCKMAP|MF_NOGRAVITY
}

mobjinfo[MT_DWZANI_HANSEG] = {
    doomednum = -1,
    spawnstate = S_ANITAIL,
    radius = 24*FRACUNIT,
    mass = 8*FRACUNIT,
    flags = MF_NOCLIP|MF_NOBLOCKMAP|MF_NOGRAVITY
}

-- Define States
states[S_ANIBODY_IDLE] = {
    sprite = SPR_EPRS,
    frame = 0,
    tics = 1,
    action = nil,
    nextstate = S_ANIBODY_IDLE
}

states[S_ANITAIL] = {
	sprite = SPR_ETAL,
	frame = 0,
	tics = -1,
	action = nil,
	nextstate = S_SEGMENT,
}

states[S_ANIHAND] = {
    sprite = SPR_ECLW,
    frame = 0,
    tics = -1,
    action = nil,
    nextstate = S_ANIHAND,
}