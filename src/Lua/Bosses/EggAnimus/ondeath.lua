//this will force exit level to MAPB0 when the Egg Animus is defeated. If you intend to use the egg animus outside of mrce, make sure to edit line 12 to suit your needs
//the second phase in the soc with the rest of the boss. Or you could adapt this script for your own use, I don't care.
//The purpose of this script is to skip the score tally screen and move straight to Dimension Warp when the boss is defeated.

local disruption = 7 * TICRATE  --initialize animation ticker
addHook("BossThinker", function(mo)
if not (mo.valid) return end  --if object was nuked by unexpected outside influence, then prevent console errors
	if mo.type == MT_EGGANIMUS_EX and mo.health <= 0  --if he ded
		disruption = $ - 1 --start animation ticker
		if disruption <= 0 --if ticker reaches 0
			disruption = 10 * TICRATE  --reset ticker
			G_SetCustomExitVars(164,1)
			G_ExitLevel()  --send us to dwz
		end
	end
end)

addHook("MapLoad", function(p, v)
	if disruption != 7 * TICRATE then  --if ticker got cut off early
		disruption = 0  --reset it
	end
end)

addHook("ThinkFrame", do
	if mapheaderinfo[gamemap].lvlttl != "Mystic Realm" then return end
	for p in players.iterate()
		if p.riftbreak == nil then
			p.riftbreak = 0
		end
		if disruption < 5*TICRATE and disruption > 0
			p.viewrollangle = p.riftbreak
		end
		if disruption == 6 * TICRATE then
			S_StartSound(nil, sfx_rumble)
			P_StartQuake(70*FRACUNIT, 110)
		end
		if disruption <= 6 * TICRATE
		and disruption >= 17 then
			p.powers[pw_nocontrol] = 1
			p.powers[pw_flashing] = 1
			p.mo.state = S_PLAY_EDGE 
		end
		if disruption == 3 * TICRATE then
			S_StartSound(nil, sfx_rumble)
			S_StartSound(nil,  sfx_rail1)
			p.riftbreak = P_RandomRange(-70,70)*ANG1
			P_FlashPal(p, 5, 15)
		end
		if disruption == 2 * TICRATE then
			S_StartSound(nil,  sfx_rail1)
			p.riftbreak = P_RandomRange(-70,70)*ANG1
			P_FlashPal(p, 5, 15)
		end
		if disruption == 1 * TICRATE then
			S_StartSound(nil,  sfx_rail1)
			p.riftbreak = P_RandomRange(-70,70)*ANG1
			P_FlashPal(p, 5, 15)
		end
		if disruption == 16 and p.mo.skin != "adventuresonic" and p.mo.skin != "sms" then
			S_StartSound(p.mo,  sfx_s3k46)
			P_DoSuperTransformation(p, true)  --super transform before it's too late
		end
		if disruption == 10 then
			S_StartSound(nil,  sfx_s3k4e)  --right before level warp, do a big blast
		end
	end
end)

addHook("NetVars", function(net)
	disruption = net($)  --net sync that shit
end)