freeslot(
    "S_GOOPERCRAWLA_STND",
    "S_GOOPERCRAWLA_RUN1",
    "S_GOOPERCRAWLA_RUN2",
    "S_GOOPERCRAWLA_RUN3",
    "S_GOOPERCRAWLA_RUN4",
    "S_GOOPERCRAWLA_RUN5",
    "S_GOOPERCRAWLA_RUN6",
    "S_GOOPERCRAWLA_RUN7",
    "SPR_GPOS"
)

states[S_GOOPERCRAWLA_STND] = {
    sprite = GPOS,
    frame = A,
    tics = 5,
    nextstate = S_GOOPERCRAWLA_STND,
    Action = A_Look
}

states[S_GOOPERCRAWLA_RUN1] = {
    sprite = GPOS,
    frame = A,
    tics = 3,
    nextstate = S_GOOPERCRAWLA_RUN2,
    Action = A_Chase
}

states[S_GOOPERCRAWLA_RUN2] = {
    sprite = GPOS,
    frame = B,
    tics = 3,
    nextstate = S_GOOPERCRAWLA_RUN3,
    Action = A_Chase
}

states[S_GOOPERCRAWLA_RUN3] = {
    sprite = GPOS,
    frame = C,
    tics = 3,
    nextstate = S_GOOPERCRAWLA_RUN4,
    Action = A_Chase
}

states[S_GOOPERCRAWLA_RUN4] = {
    sprite = GPOS,
    frame = D,
    tics = 3,
    nextstate = S_GOOPERCRAWLA_RUN5,
    Action = A_Chase
}

states[S_GOOPERCRAWLA_RUN5] = {
    sprite = GPOS,
    frame = E,
    tics = 3,
    nextstate = S_GOOPERCRAWLA_RUN6,
    Action = A_Chase
}

states[S_GOOPERCRAWLA_RUN6] = {
    sprite = GPOS,
    frame = F,
    tics = 3,
    nextstate = S_GOOPERCRAWLA_RUN7,
    Action = A_Chase
}
    
states[S_GOOPERCRAWLA_RUN7] = {
    sprite = GPOS,
    frame = F,
    tics = 0,
    nextstate = S_GOOPERCRAWLA_RUN1,
    Action = A_DropMine
}

mobjinfo[MT_GOOPERCRAWLA] = {
    --$Name Gooper Crawla
    --$Sprite GPOSA1
    --$Category Mystic Realm Enemies
    doomednum = 3101,
    SpawnState = S_GOOPERCRAWLA_STND,
    SpawnHealth = 1,
    SeeState = S_GOOPERCRAWLA_RUN1,
    SeeSound = sfx_None,
    AttackSound = sfx_None,
    PainState = S_NULL,
    PainSound = sfx_None,
    MeleeState = S_NULL,
    MissileState = S_NULL,
    DeathState = S_XPLD1,
    XDeathState = S_NULL,
    DeathSound = sfx_pop,
    ActiveSound = sfx_None,
    RaiseState = MT_SLOWGOOP,
    ReactionTime = 35,
    PainChance = 200,
    Speed = 8,
    Radius = 24*FRACUNIT,
    Height = 32*FRACUNIT,
    DispOffset = 0,
    Mass = 100,
    Damage = 0,
    Flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE
}