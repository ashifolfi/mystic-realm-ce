/* 
	MRCE Custom Intermission

	STJr (Bonus calc functions)
	AshiFolfi (C -> Lua and everything else)
*/

-- I just realized this is heavily netunsafe and should be rewritten to attach bonus_t to the player

-- Variables we use during the intermission formatted into a table
---@class bonus_t
local bonus_t = {
	tpoints = 0, -- time
	rpoints = 0, -- rings
	gpoints = 0, -- guard

	nummaprings = 0,
	perfectbonus = 0,

	prevscore = 0,
	newadd = 0,
	intcomplete = false,

	animticker = 0,
	display = false
}

-- Function used to calculate the total number of rings in a map
---@param mobj mobj_t
local function Y_CalcTotalRings(mobj)
	-- For every instance of a ring add 1
	if (mobj.type == MT_RING)
	or (mobj.type == MT_COIN)
	or (mobj.type == MT_NIGHTSSTAR) then
		bonus_t.nummaprings = $ + 1
	end
	-- For every instance of a ring box add 10
	if (mobj.type == MT_RING_BOX) then
		bonus_t.nummaprings = $ + 10
	end
	-- If NiGHTS drone is present disable perfect bonus
	-- this is vanilla behavior
	if (mobj.type == MT_NIGHTSDRONE) then
		bonus_t.perfectbonus = -1
	end
end
-- Put inside of a MobjSpawn so that any rings added mid game are accounted for
addHook("MobjSpawn", Y_CalcTotalRings)

-- Intermission bonus calc functions

-- Used to calculate the time bonus. C -> Lua Conversion
---@param player player_t
---@param bstruct table
local function Y_SetTimeBonus(player, bstruct)
	local secs, bonus

	-- calculate time bonus
	local secs = player.realtime / TICRATE
	if      (secs <  30) then /*   :30 */ bonus = 50000
	elseif (secs <  60) then /*  1:00 */ bonus = 10000
	elseif (secs <  90) then /*  1:30 */ bonus = 5000
	elseif (secs < 120) then /*  2:00 */ bonus = 4000
	elseif (secs < 180) then /*  3:00 */ bonus = 3000
	elseif (secs < 240) then /*  4:00 */ bonus = 2000
	elseif (secs < 300) then /*  5:00 */ bonus = 1000
	elseif (secs < 360) then /*  6:00 */ bonus = 500
	elseif (secs < 420) then /*  7:00 */ bonus = 400
	elseif (secs < 480) then /*  8:00 */ bonus = 300
	elseif (secs < 540) then /*  9:00 */ bonus = 200
	elseif (secs < 600) then /* 10:00 */ bonus = 100
	else  /* TIME TAKEN: TOO LONG */ bonus = 0 end

	bstruct.tpoints = bonus
end

-- Calculates the ring bonus. C -> Lua Conversion
---@param player player_t
---@param bstruct table
local function Y_SetRingBonus(player, bstruct)
	bstruct.rpoints = max(0, (player.rings) * 100);
end

-- Calculates the perfect bonus and displays it if grabbed
---@param player player_t
---@param bstruct bonus_t
local function Y_SetPerfectBonus(player, bstruct)
	-- If the number of rings in a map is 0 there will be no perfect bonus
	if bstruct.nummaprings == 0 then return end
	-- If perfectbonus is set to -1 (map should not have perfect bonus)
	-- Do not run the perfect bonus
	if bstruct.perfectbonus == -1 then return end

	if (player.rings >= bstruct.nummaprings) then
		bstruct.perfectbonus = 5000 -- It's always 5000. No more. No less.
	end
end

-- Calculates the guard bonus for boss stages
---@param player player_t
---@param bstruct bonus_t
local function Y_SetGuardBonus(player, bstruct)
	local bonus = 0
	-- We don't have access to stagefailed. Shouldn't be that important right?
		if     (player.timeshit == 0) then bonus = 10000
		elseif (player.timeshit == 1) then bonus = 5000
		elseif (player.timeshit == 2) then bonus = 1000
		elseif (player.timeshit == 3) then bonus = 500
		elseif (player.timeshit == 4) then bonus = 100
		else                            bonus = 0
		end

	bstruct.gpoints = bonus;
end

-- Actual intermission work

-- Here is the full table of what level types determine what bonuses
local bfunc_t = {
	[-1] = function() -- None
		return
	end,
	[0] = function(player) -- Normal
		Y_SetTimeBonus(player, bonus_t)
		Y_SetRingBonus(player, bonus_t)
		Y_SetPerfectBonus(player, bonus_t)
	end,
	[1] = function(player) -- Boss
		Y_SetGuardBonus(player, bonus_t)
		Y_SetRingBonus(player, bonus_t)
	end,
	[2] = function(player) -- ERZ3
		Y_SetGuardBonus(player, bonus_t)
		Y_SetRingBonus(player, bonus_t)
	end,
}

-- Stop the timer and disable player collisions then calculate our bonuses.
-- Also makes sure to skip the vanilla intermission and block it's score calculations.
---@param player player_t
local function hud_intermission_backend(player)
	if not(player.exiting) then return end

	if (bonus_t.intcomplete == false) then
		bonus_t.display = true
		-- don't exit the level until we want to
		-- (also skip real intermission lol)
		player.exiting = 5

		-- exit the level and skip the intermission.

		-- Perform calculations based on bonustype
		bfunc_t[mapheaderinfo[gamemap].bonustype](player)

		-- Set our current score and how much to add to it
		bonus_t.prevscore = player.score
		bonus_t.newadd = bonus_t.rpoints + bonus_t.tpoints + bonus_t.gpoints

		-- Add 1 to the current score every tic until we reach newadd
		if not(player.score == bonus_t.prevscore + bonus_t.newadd) then
			player.score = $ + 1
		end
	else
		bonus_t.display = false
		player.exiting = 5
	end
end
addHook("PlayerThink", hud_intermission_backend)

-- Sample frontend code based on vanilla SRB2 intermission