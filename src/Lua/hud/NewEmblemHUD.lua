freeslot("SPR_UNCEMB")
local emblemlist = {}
local offset = 320
local offsetresettimer = 0
local tabresettimer = 2
local touchedresettimer = 25
local tabMenuOpened = nil

addHook("PlayerSpawn", function()
	emblemlist = {}
	for mapemblem in mobjs.iterate() do
		if mapemblem.valid and mapemblem.type == MT_EMBLEM then
			local emblem = {frame = mapemblem.frame, color = mapemblem.color, origmobj = mapemblem}
			table.insert(emblemlist, emblem)
		end
	end
end)

addHook("TouchSpecial", function(emblem, pmo)
	pmo.player.changeoffset = 1
	offsetresettimer = touchedresettimer
end, MT_EMBLEM)

addHook("ThinkFrame", function()
	for p in players.iterate do
		if not p.bot and p.valid then
			if tabMenuOpened == true then
				p.changeoffset = 1
				offsetresettimer = 2
				tabMenuOpened = false
			end
			if p.changeoffset then
				if offset > 160 then
					offset = $ - 10
				end
				if offset == 160 and offsetresettimer then
					offsetresettimer = $ - 1
				end
				if not offsetresettimer then
					p.changeoffset = 0
				end
			else
				if offset < 320 then
					offset = $ + 20
				end
			end
		end
	end
end)

hud.add(function(d, p)
	if netgame then return end
	local loffset = offset
	local bg = d.cachePatch("EMBBG")
	d.draw(loffset, 19, bg, V_SNAPTORIGHT|V_SNAPTOTOP)
	for embnum, emblem in ipairs(emblemlist) do
		local emblempatch = d.getSpritePatch("EMBM", emblem.frame & FF_FRAMEMASK)
		local uncollectedemb = d.cachePatch("UNCEMB")
		local emblemcolor = d.getColormap(TC_DEFAULT, emblem.color)
		d.drawScaled((loffset*FRACUNIT + ((uncollectedemb.width / 4)*FRACUNIT)), 24*FRACUNIT, FRACUNIT/2, uncollectedemb, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
		if (emblem.frame & FF_TRANSMASK) or (not emblem.origmobj.valid) then
			d.drawScaled((loffset*FRACUNIT + ((emblempatch.width / 4)*FRACUNIT)), 24*FRACUNIT, FRACUNIT/2, emblempatch, V_SNAPTORIGHT|V_SNAPTOTOP, emblemcolor)
		end
		loffset = $ + 32
	end
end, "game")

hud.add(function(d, p)
	tabMenuOpened = true
	offsetresettimer = tabresettimer
end, "scores")
